---
description: Learn how to manage your Meltano project's plugins.
---

# Plugin Management

A [Meltano project](/docs/project.html)'s primary components are its [plugins](/docs/plugins.html),
that implement the various details of your ELT pipelines.

Your project's plugins are defined in your [`meltano.yml` project file](/docs/project.html#meltano-yml-project-file), and [installed](#installing-your-project-s-plugins) inside the [`.meltano` directory](/docs/project.html#meltano-directory).
They can be managed using various [CLI commands](/docs/command-line-interface.html) as well as the [UI](/docs/command-line-interface.html#ui).

## Known and custom plugins

## Adding extractors and loaders to your project

Like all types of plugins, extractors and loaders can be added to a Meltano project using [`meltano add`](/docs/command-line-interface.html#add).

Plugins that are already [known to Meltano](/docs/contributor-guide.html#known-plugins) can be added by simply specifying their `type` and `name`, while adding a plugin that Meltano isn't familiar with yet requires adding the `--custom` flag.

To find out what plugins are already known to Meltano and supported out of the box, you can use [`meltano discover`](/docs/command-line-interface.html#discover), with an optional pluralized `plugin type` argument.
You can also check out the lists of supported [extractors](/plugins/extractors/) and [loaders](/plugins/loaders/) on this website.

If the Singer tap or target you'd like to use with Meltano doesn't show up in any of these places, you're going to want to [add a custom plugin](/docs/command-line-interface.html#how-to-use-custom-plugins).

```bash
# List extractors and loaders known to Meltano
meltano discover extractors
meltano discover loaders

# Add a known extractor or loader by name
meltano add extractor tap-salesforce
meltano add loader target-snowflake

# Add an unknown (custom) extractor or loader
meltano add --custom extractor tap-covid-19
```

## Installing your project's plugins

Whenever you add a new plugin to a Meltano project using [`meltano add`](/docs/command-line-interface.html#add),
it will be installed into your project's [`.meltano` directory](/docs/project.html#meltano-directory) automatically.

However, since this directory is included in your project's `.gitignore` file
by default, you'll need to explicitly run [`meltano install`](/docs/command-line-interface.html#install)
before any other `meltano` commands whenever you clone or pull an existing Meltano project from version control,
to install (or update) all plugins specified in your [`meltano.yml` project file](/docs/project.html#meltano-yml-project-file).
